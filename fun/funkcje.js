// File without any modifications downloaded from live http://www.mos.gov.pl/p/funkcje.js (18.12.2014 12:23)
var cssWlaczony, body;
function inicjujJs() {	
	if (top.frames.length!=0 && top.frames[0].name != 'lbIframe') top.location=self.document.location; 
	body = $(document.body.getAttribute('id'));
	if (body.getStyle('unicode-bidi') == 'embed') {cssWlaczony = new Boolean(true);}
	if (cssWlaczony) {			// FUNKCJE DLA CALEGO SERWISU, PRZY NIE WLACZONYCH ULATWIENIACH
		obserwujNarzedzia(); 		// wlacza obsluge zdarzen dla menu "narzedzia" (czcionka, kontrast, drukuj)
		ustawKlase('inicjuj');		// wlacza obsluge czcionki
		dzieciol();					// dzieciol jaki jest kazdy widzi.
		if (maKlase(body, 'lan')) { // FUNKCJE TYLKO DLA LAN'U

		} else {					// FUNKCJE TYLKO DLA INTERNETU
			bipRejestr();				// pokaz ukryj tabele rejestru zmian w bip'ie
			animujSzybkieMenu(); 		// wlacza animacje dla szybkiego menu rozwijanego
			cssMenu(); 					// menu glowne rozwijane z dodatkami graficznymi, patch dla ie6
			animujBannery(); 			// wlacza alpha dla bannerow			
		}
	}							// FUNKCJE DLA CALEGO SERWISU NIEZALEZNE OD MOTYWU GRAFICZNEGO
	if (maKlase(body, 'lan')) {
		komunikat(); 				// wlacza komunikat po wejsciu na strone intranetu		
	} else {
		komunikatInternet();   		// wlacza komunikat po wejsciu na strone internetu		
	}
	inputyZTekstem();				// nadaje wartosc domyslna polom input (zdefiniowanym wewnatrz funkcji)
	accordion('archiwum');			// archwium przegladu prasy (rozwijane lata)
	pokazUkryjElementy();			// po kliknieciu w element o klasie .pokazUkryj nastepny div sie odsloni/zasloni.
	if (!cssWlaczony) {
		myLytebox = null; 			// "normalne" zachowanie linkow, gdy wlaczono ulatwienia dostepu i lytebox nie zadziala.
	}
}

function PierdyknijOkienkoCookies()
{
   document.getElementById('divcook').style.display="none";
      document.cookie = "cookiemosworn=true; path=/; max-age=2592000;";
      }

var cookietext='<div id="divcook"><div id="textcook">Używamy plików cookies, aby ułatwić Ci korzystanie z naszego serwisu oraz do celów statystycznych. Jeśli nie blokujesz tych plików, to zgadzasz się na ich użycie oraz zapisanie w pamięci urządzenia. Pamiętaj, że możesz samodzielnie zarządzać cookies, zmieniając ustawienia przeglądarki.</a><div onclick="PierdyknijOkienkoCookies()" style="cursor: pointer; text-align: center; font-weight: bold;">Zamknij</div></div></div>';



function noweOkno(url, nazwa, szerokosc, wysokosc) {
	x = (screen.width) ? (screen.width - szerokosc) / 2 : 0;
	y = (screen.height) ? (screen.height - wysokosc) / 2 : 0;
	window.open(url, nazwa, 'toolbar=0,location=0,directories=0,menubar=0,status=0,scrollbars=1,resizable=1,left=' + x + ',top=' + y + ',width=' + szerokosc + ',height=' + wysokosc);
}





function dzieciol() {
	var warstwa=$('dzieciol');
	if (warstwa) {
		body.addEvent('mousemove', function(event){
			warstwa.setStyles({
				left : event.page.x+15,
				top : event.page.y+20
			});
		});
	}

}




function pokazUkryjElementy() {
	$$('.pokazUkryj').each(function(item,index) {
		item.setStyle('cursor','pointer');
		//var tekst = item.innerHTML;
		//item.innerHTML = 'pokaż '+tekst;
		item.addEvent('click', function() {  
			if (item.getNext('div').style.display == 'block') {
				item.getNext('div').style.display = 'none';
				//item.innerHTML = 'pokaż '+tekst;
			} else {
				item.getNext('div').style.display = 'block';
				//item.innerHTML = 'ukryj '+tekst;
			}
		});
	});	
}

function accordion(pojemnik) {
	if ($(pojemnik)) {
		var myAccordion = new Accordion($(pojemnik), 'dt', 'dd', {
			opacity: false,
			onActive: function(toggler, element){
				toggler.setStyle('background-color', '#BBD631');
			},
			onBackground: function(toggler, element){
				toggler.setStyle('background-color', '#CCCCCC');
			}
		});	
		}	
}

function inputyZTekstem() {
	if ($('newsletterEmail')) {$('newsletterEmail').setStyle('text-align', 'center')}; // ale "left" jesli :focus
	if ($('newsletterEmailEn')) {$('newsletterEmailEn').setStyle('text-align', 'center')}; // ale "left" jesli :focus
	var pola = new Array();


	pola['inputLogin'] = 		'Login';	
	pola['inputHaslo'] = 		'*****';
	pola['newsletterEmail'] = 	'Twój e-mail';	// to jedno pole ma miec wysrodkowane value, gdy nieaktywne.
	pola['newsletterEmailEn'] = 'Your e - mail';
	pola['captcha-form'] = 		'Tekst z obrazka';
	pola['subskrypcjaEmail'] = 	'Twój e-mail';
	pola['wyszukiwarkaWEB20'] =	'Szukaj';
	
	for (var el in pola) {
		ustawValue(el,pola[el]);
	}
}
function ustawValue(id,Value) {
	if ($(id)) { 
		if ($(id).value == '') {
			$(id).value = Value;
		} else { // jezeli input ma "value" bezposrednio ustawione, to staje sie domyslne.
			Value = $(id).value;
		}
		$(id).addEvents({
			'focus': function() {
				if ($(id).value == Value) {
					if ((id == 'newsletterEmail') || (id == 'newsletterEmailEn')) {
						$(id).setStyle('text-align', 'left');
					}
					$(id).value = '';
				}
			},
			'blur': function() {
				if ($(id).value == '') {
					if ((id == 'newsletterEmail') || (id == 'newsletterEmailEn')) {
						$(id).setStyle('text-align', 'center');
					}	
					$(id).value = Value;
				}
			}
		});
	}
}




function animujBannery() {
	$$('.bannery').each(function(item, index){
		item.fade(0.5);
		item.addEvents({
			'mouseenter': function(e) {
				e.stop();
				this.fade(1);				
			},
			'mouseleave': function(e) {
				e.stop();
				this.fade(0.5);				
			}
		});	
	});
}






function cssMenu() {
	if ($('menu') && window.attachEvent) {
		var menu = $('menu'),
			kategorieGlowne = new Array(),
			podkategorie = new Array(),
			elementy = menu.getElementsByTagName('LI');
		for (var i=0; i<elementy.length; i++) {
			if (elementy[i].parentNode.tagName == 'OL') {
				kategorieGlowne.push(elementy[i]);
			} else {
				podkategorie.push(elementy[i]);
			}
		}
		kategorieGlowne.each(function(item,index) {
			$(item).addEvents({
					'mouseenter': function() {
						item.addClass('podswietlone');
					},
					'mouseleave': function() {
						item.removeClass('podswietlone');
					}
			});
		});	
		podkategorie.each(function(item,index) {
			$(item).addEvents({
					'mouseenter': function() {
						item.addClass('subpodswietlone');
					},
					'mouseleave': function() {
						item.removeClass('subpodswietlone');
					}
			});
		});		
	}
}



var myVerticalSlide, myVerticalSlide2;
function animujSzybkieMenu() {
	if ($('v_slidein')) {
		myVerticalSlide = new Fx.Slide('vertical_slide');
		myVerticalSlide.hide();
		$('v_slidein').addEvents({
			'mouseenter': function(e) {
				e = new Event(e);
				e.stop();
				myVerticalSlide.slideIn();			
			},
			'mouseleave': function(e) {
				e.stop();
				setTimeout('myVerticalSlide.slideOut()', 700);
			}
		});	
	}
	if ($('v_slidein2')) {
		myVerticalSlide2 = new Fx.Slide('vertical_slide2');
		myVerticalSlide2.hide();
		$('v_slidein2').addEvents({
			'mouseenter': function(e) {
				e = new Event(e);
				e.stop();
				myVerticalSlide2.slideIn();			
			},
			'mouseleave': function(e) {
				e.stop();
				setTimeout('myVerticalSlide2.slideOut()', 700);
			}
		});	
	}	
}




function obserwujNarzedzia() {
		$$('.powiekszTekst').each(function(item,index) {
			item.addEvent('click', function() {  
				ustawKlase('powieksz');
			});
		});	
		$$('.zmniejszTekst').each(function(item,index) {
			item.addEvent('click', function() {  
				ustawKlase('zmniejsz');
			});
		});	
		$$('.resetujTekst').each(function(item,index) {
			item.addEvent('click', function() {  
				ustawKlase('resetuj');
			});
		});
	
		$$('.drukuj').each(function(item,index) {
			item.addEvent('click', function() {  
				if (item.get('href')) {
					window.open(item.get('href'), 'okno_druku', 'menubar=1,scrollbars=1,resizable=1,width=773,height=650'); return false;
				} else {
					window.print();
				}
			});
		});
		
}


function tworzCiacho(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}


function ustawKlase(czynnosc) {
	var klasaCssWartosc = Cookie.read('klasaCss');
	if (klasaCssWartosc != null) { 
		klasaCssWartosc = parseInt(klasaCssWartosc);
	}

	switch (czynnosc) {
		case 'powieksz':
			if (klasaCssWartosc+1 < 11) {
				body.removeClass('fs'+klasaCssWartosc);
				klasaCssWartosc++;
				body.addClass('fs'+klasaCssWartosc);
				tworzCiacho("klasaCss", klasaCssWartosc, 2);
			}
		break;
		case 'zmniejsz':
			if (klasaCssWartosc-1 > 3) {
				body.removeClass('fs'+klasaCssWartosc);
				klasaCssWartosc--;
				body.addClass('fs'+klasaCssWartosc);
				tworzCiacho("klasaCss", klasaCssWartosc, 2);
			}
		break;
		case 'inicjuj':
				if (klasaCssWartosc == null) {
					klasaCssWartosc = 4;
				}
				body.addClass('fs'+klasaCssWartosc);
				tworzCiacho("klasaCss", klasaCssWartosc, 2);		
		break;	
		case 'resetuj':
				body.removeClass('fs'+klasaCssWartosc);		
				klasaCssWartosc = 4;
				body.addClass('fs'+klasaCssWartosc);
				tworzCiacho("klasaCss", klasaCssWartosc, 2);
		break;	
		default:
			body.addClass('fs4');
			klasaCssWartosc = 4;
			tworzCiacho("klasaCss", klasaCssWartosc, 2);	
	}
}


function bipRejestr() {
	if ($('rejestr_table')) {
		$('pokaz_rejestr').addEvent('click', function() {  
			pokazUkryjRejestrZmian();
		});
		$('rejestr_table').setStyle('display', 'none');
	}
}



function pokazUkryjRejestrZmian() {
	if ($('rejestr_table').style.display == 'none') {
		$('rejestr_table').setStyle('display', 'block');
		//$('pokaz_rejestr').innerHTML = 'Ukryj historię zmian';
	} else {
		$('rejestr_table').setStyle('display', 'none');
		//$('pokaz_rejestr').innerHTML = 'Pokaż historię zmian';	
	}
}  

function komunikat() {
	var handle = Cookie.read('komunikat');
	if( !handle && $('komunikat')) {
		var myRequest = new Request({
							method: 'get', 
							url: '/komunikat/',
							onSuccess : function(aOptions)
							{
								if( aOptions != '' )
								{
									myLytebox = new LyteBox('komunikat');
									if (typeof myLytebox != 'undefined') {
										myLytebox.start($('komunikat'), false, true);
									}
								}
							}
		        	}).send();
		
		/*cookie  = Cookie.write('komunikat', '1', {duration: 1, path: '/'});*/      	
	}
}

function komunikatInternet() {
	var handle = Cookie.read('komunikatInternet');
	if( !handle && $('komunikatInternet')) {
		var myRequest = new Request({
							method: 'get', 
							url: '/komunikat/',
							onSuccess : function(aOptions)
							{
								if( aOptions != '' )
								{
									myLytebox = null;
									myLytebox = new LyteBox('komunikat');
									if (typeof myLytebox != 'undefined') {
										myLytebox.start($('komunikatInternet'), false, true);
										$('komunikatUlatwienia').setAttribute('href', '#lbIframe');
										$('komunikatUlatwienia').appendText('Przeczytaj komunikat Ministerstwa');
									}
								}
							}
		        	}).send('typ=internet');
		/*cookie  = Cookie.write('komunikatInternet', '1', {duration: 1, path: '/'});*/      	
	}
}

function pokaz(idTekstu){
	var element = document.getElementById(idTekstu);
	if(element.style.display == 'block')
	element.style.display = 'none';
	else
	element.style.display = 'block';
	}

function maKlase (obj, klasaCss) {
    if (typeof obj == 'undefined' || obj==null || !RegExp) { return false; }
    var re = new RegExp("(^|\\s)" + klasaCss + "(\\s|$)");
    if (typeof(obj)=="string") {
      return re.test(obj);
    } else if (typeof(obj)=="object" && obj.className) {
      return re.test(obj.className);
    }
	return false;
}
